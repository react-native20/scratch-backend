"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "folders",
      [
        {
          title: "First Folder",
          created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        },
        {
          title: "Second Folder",
          created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("folders", null, {});
  },
};
