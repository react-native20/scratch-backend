"use strict";

const { User, Folder } = require("../../models");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const users = await User.findAll();
    const folders = await Folder.findAll();

    await queryInterface.bulkInsert(
      "notes",
      [
        {
          user_id: users[0].id,
          folder_id: folders[0].id,
          title: "First Test Note",
          body: "Lorem ipsum test note body",
          created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        },
        {
          user_id: users[0].id,
          folder_id: folders[0].id,
          title: "Second Test Note",
          body: "Lorem ipsum test note body",
          created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        },
        {
          user_id: users[0].id,
          folder_id: folders[1].id,
          title: "Third Test Note",
          body: "Lorem ipsum test note body",
          created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     */
    await queryInterface.bulkDelete("notes", null, {});
  },
};
