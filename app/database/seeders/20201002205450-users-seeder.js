"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    Example: await queryInterface.bulkInsert(
      "users",
      [
        {
          name: "John Doe",
          username: "username",
          password: "password",
          created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("users", null, {});
  },
};
