const router = require("express").Router();

const userController = require("../controllers/user.controller");

router.get("/users", userController.index);
router.get("/users/:userId", userController.show);
router.post("/users", userController.store);
router.patch("/users/:userId", userController.update);
router.delete("/users/:userId", userController.destroy);

module.exports = router;
