var router = require("express").Router();

const notesController = require("../controllers/note.controller.js");

// user authentication
router.use(require("../middleware/authenticate"));

router.get("/notes", notesController.index);
router.get("/notes/:noteId", notesController.show);
router.post("/notes", notesController.store);
router.patch("/notes/:noteId", notesController.update);
router.delete("/notes/:noteId", notesController.destroy);

module.exports = router;
