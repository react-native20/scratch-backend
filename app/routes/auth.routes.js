var router = require("express").Router();

const authController = require("../controllers/auth.controller.js");

router.post("/register", authController.register);
router.post("/login", authController.login);
// router.get("/notes/:noteId", authController.show);
// router.post("/notes", authController.store);
// router.patch("/notes/:noteId", authController.update);
// router.delete("/notes/:noteId", authController.destroy);

module.exports = router;
