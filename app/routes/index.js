module.exports = (app) => {
  const PREFIX = "/api/v1";

  app.use(PREFIX, require("./auth.routes"));
  app.use(PREFIX, require("./note.routes"));
  app.use(PREFIX, require("./folder.routes"));
  app.use(PREFIX, require("./user.routes"));
};
