var router = require("express").Router();

const foldersController = require("../controllers/folder.controller.js");

// user authentication
router.use(require("../middleware/authenticate"));

router.get("/folders", foldersController.index);
router.get("/folders/:folderId", foldersController.show);
router.post("/folders", foldersController.store);
router.post("/folders/:folderId", foldersController.update);
router.delete("/folders/:folderId", foldersController.destroy);

module.exports = router;
