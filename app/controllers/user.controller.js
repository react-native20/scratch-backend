const bcrypt = require("bcrypt");

const { User } = require("../models");

// fetch all users
exports.index = (req, res) => {
  User.findAll()
    .then((users) => res.json(users))
    .catch((err) => res.json({ message: err.message }));
};

// fetch one user
exports.show = (req, res) => {
  const { userId } = req.params;

  User.findByPk(userId)
    .then((user) => {
      return user
        ? res.json(user)
        : res.json({ message: `No user found with id of ${userId}` });
    })
    .catch((err) => res.json({ message: err.message }));
};

// create a new user
exports.store = (req, res) => {
  const { name, username, password } = req.body;
  if (!name) {
    return res.json({ message: "Name cannot be null." });
  }
  if (!username) {
    return res.json({ message: "Usernamecannot be null." });
  }
  if (!password) {
    return res.json({ message: "Password cannot be null." });
  }

  bcrypt.hash(req.body.password, 10, (err, hash) => {
    if (err) return res.json({ message: "There was an error creating user." });
    const user = {
      name: req.body.name,
      username: req.body.username,
      password: hash,
    };
    User.create(user)
      .then((user) => res.json(user))
      .catch((err) => res.status(500).json({ message: err.message }));
  });
};

// update an existing user
exports.update = (req, res) => {
  const id = req.params.userId;

  User.update(req.body, {
    where: { id },
  })
    .then((rowsAffected) => {
      return rowsAffected == 1
        ? res.json({ message: "Successfully updated user." })
        : res.json({
            message: `Could not find user with id of ${id}.`,
          });
    })
    .catch((err) => res.json({ message: err.errors[0].message }));
};

// delete an user
exports.destroy = (req, res) => {
  const id = req.params.userId;

  User.destroy({ where: { id } })
    .then((rowsAffected) => {
      return rowsAffected == 1
        ? res.json({ message: `Successfully deleted user with id of ${id}` })
        : res.json({ message: `Could not find user with id of ${id}` });
    })
    .catch((err) => res.json({ message: err.message }));
};
