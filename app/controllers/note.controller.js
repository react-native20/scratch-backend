const { Note } = require("../models/index");

// Get all notes
exports.index = (req, res) => {
  Note.findAll()
    .then((notes) => res.json(notes))
    .catch((err) => res.status(500).json({ message: err.message }));
};

// Get specific note
exports.show = (req, res) => {
  const id = req.params.noteId;

  Note.findByPk(id, {
    include: "folder",
  })
    .then((note) => {
      return note
        ? res.json(note)
        : res.json({ message: `Could not find note with id of ${id}` });
    })
    .catch((err) => res.status(500).json({ message: err.message }));
};

// Store a note
exports.store = (req, res) => {
  Note.create(req.body)
    .then((results) => res.json(results))
    .catch((err) => {
      res.status(500).json({
        message: err.message,
      });
    });
};

// Update a note
exports.update = (req, res) => {
  const id = req.params.noteId;

  Note.update(req.body, {
    where: { id },
  })
    .then((rowsAffected) => {
      return rowsAffected == 1
        ? res.json({ message: "Successfully updated note." })
        : res.json({
            message: `Could not find note with id of ${id}.`,
          });
    })
    .catch((err) => res.json({ message: err.message }));
};

// Delete a note
exports.destroy = (req, res) => {
  const id = req.params.noteId;

  Note.destroy({ where: { id } })
    .then((rowsAffected) => {
      return rowsAffected == 1
        ? res.json({ message: `Successfully deleted note with id of ${id}` })
        : res.json({ message: `Could not find note with id of ${id}` });
    })
    .catch((err) => res.json({ message: err.message }));
};
