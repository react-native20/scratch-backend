const { Folder } = require("../models/index");

// Get all folders
exports.index = (req, res) => {
  Folder.findAll()
    .then((folders) => res.json(folders))
    .catch((err) => res.status(500).json({ message: err.message }));
};

// Get specific folder
exports.show = (req, res) => {
  const id = req.params.folderId;

  Folder.findByPk(id, { include: "notes" })
    .then((folder) => {
      return folder
        ? res.json(folder)
        : res.json({ message: `Could not find folder with id of ${id}` });
    })
    .catch((err) => res.status(500).json({ message: err.message }));
};

// Store a folder
exports.store = (req, res) => {
  const folder = {
    title: req.body.title,
  };
  Folder.create(folder)
    .then((results) => res.json(results))
    .catch((err) =>
      res.status(500).json({
        message: err.message,
      })
    );
};

// Update a folder
exports.update = (req, res) => {
  const id = req.params.folderId;

  Folder.update(req.body, {
    where: { id },
  })
    .then((rowsAffected) => {
      return rowsAffected == 1
        ? res.json({ message: "Successfully updated folder." })
        : res.json({
            message: `Could not find a folder with id of ${id}.`,
          });
    })
    .catch((err) => res.json({ message: err.message }));
};

// Delete a folder
exports.destroy = (req, res) => {
  const id = req.params.folderId;

  Folder.destroy({ where: { id } })
    .then((rowsAffected) => {
      return rowsAffected == 1
        ? res.json({ message: `Successfully deleted folder with id of ${id}` })
        : res.json({ message: `Could not find a folder with id of ${id}` });
    })
    .catch((err) => res.json({ message: err.message }));
};
