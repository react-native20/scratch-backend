const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const { User } = require("../models");

// register a new user
exports.register = (req, res) => {
  const { username, password } = req.body;

  // validate the request body
  if (!username || !password) {
    return res.json({ message: "Username and password cannot be empty." });
  }

  User.findOne({ where: { username } }).then((user) => {
    // username taken
    if (user) return res.json({ message: "Sorry! Username already taken." });
    // register the user
    bcrypt.hash(password, 10, (err, hash) => {
      if (err)
        return res.json({ message: "There was an error creating user." });
      const user = {
        name: req.body.name,
        username: req.body.username,
        password: hash,
      };
      User.create(user)
        .then((user) => res.json(user))
        .catch((err) => res.status(500).json({ message: err.message }));
    });
  });
};

// authenticate a user
exports.login = (req, res) => {
  User.findOne({ where: { username: req.body.username } })
    .then(async (user) => {
      // no user found
      if (!user)
        return res.json({
          message: "Incorrect username or password. Debug: username",
        });
      // user found, compare passwords
      const passwordsMatch = await bcrypt.compare(
        req.body.password,
        user.password
      );
      if (passwordsMatch) {
        // generate and return authToken
        const authToken = jwt.sign(
          { id: user.id, username: user.username },
          process.env.ACCESS_TOKEN_SECRET
        );
        res.send(authToken);
      } else {
        res.json({
          message: "Incorrect username or password. Debug: password",
        });
      }
    })
    .catch((err) => res.json({ message: err.message }));
};
