"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  // define model class
  class Folder extends Model {
    static associate(models) {
      this.hasMany(models.Note, {
        as: "notes",
        foreignKey: "folder_id",
        onDelete: "CASCADE",
        onUpdate: "NO ACTION",
      });
    }
  }

  // initialize model
  Folder.init(
    {
      title: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Folder",
      underscored: true,
    }
  );

  return Folder;
};
