"use strict";

const Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  // define the model
  class User extends Sequelize.Model {
    static associate(models) {
      this.hasMany(models.Note, {
        as: "notes",
        foreignKey: "user_id",
        onDelete: "CASCADE",
        onUpdate: "NO ACTION",
      });
    }
  }

  // initialize model
  User.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      username: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "User",
      underscored: true,
    }
  );

  return User;
};
