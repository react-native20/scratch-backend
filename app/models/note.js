"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  // define model class
  class Note extends Model {
    static associate(models) {
      this.belongsTo(models.User, {
        as: "user",
        foreignKey: "user_id",
      });
      this.belongsTo(models.Folder, {
        as: "folder",
        foreignKey: "folder_id",
      });
    }
  }

  // initialize model
  Note.init(
    {
      user_id: DataTypes.INTEGER,
      folder_id: DataTypes.INTEGER,
      title: DataTypes.STRING,
      body: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Note",
      underscored: true,
    }
  );

  return Note;
};
