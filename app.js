require("dotenv").config();

var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

const { sequelize } = require("./app/models");

// test the datbase connection
sequelize
  .authenticate()
  .then(() => console.log("Database connection established successfully."))
  .catch((err) => console.log("Unable to connect to the database.", err));

/* strip trailing slashes from url */
app.use(function (req, res, next) {
  if (req.path.substr(-1) == "/" && req.path.length > 1) {
    let query = req.url.slice(req.path.length);
    res.redirect(301, req.path.slice(0, -1) + query);
  } else {
    next();
  }
});

// register routes with the app
require("./app/routes")(app);

module.exports = app;
